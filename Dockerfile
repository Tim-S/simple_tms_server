FROM osgeo/gdal:alpine-normal-latest
ADD watch_maps.py /usr/bin/watch_maps.py
ADD simple_cors_server.py /usr/bin/simple_cors_server.py
ADD run.sh /root/run.sh

EXPOSE 8000
ENTRYPOINT ["/root/run.sh"]
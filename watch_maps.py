#!/usr/bin/env python3
# encoding: utf-8
import sys
import time
import os

if __name__ == "__main__":
    src_path = sys.argv[1] if len(sys.argv) > 1 else '.'
    src_path = os.path.abspath(src_path)
    print("Watching", src_path, " for untiled Maps...")
    while True:
        for filename in os.listdir(src_path):
            abs_filepath = os.path.abspath(os.path.join(src_path, filename))
            if abs_filepath.endswith(".tif"):
                abs_filepath_wo_ext, ext = os.path.splitext(abs_filepath)
                if not os.path.exists(abs_filepath_wo_ext):
                    print("Found new map :", abs_filepath)
                    os.system(f"gdal2tiles.py --webviewer=leaflet {abs_filepath} {abs_filepath_wo_ext}")

                continue
            else:
                continue
        time.sleep(2)
# How to Setup a Very Basic TMS Server

This setup demonstrates how to automate tiling geotif's in a folder and publish them as a TMS Source via a SimpleHTTPServer 

## Getting Started

The most simple way to get a simple TMS Server up is to simply clone this repo and run
```sh
docker-compose up
```

And place some geotiff (e.g. bluemarble.tif) into the _maps_ directory. 

The geotiff will be tiled with gdal2tiles  automatically (gdal default settings).
For each _{filename}.tif_ file in _maps_ a folder named _{filename}_ is created for the tiles. 
The created folder also contains a leaflet webviewer for demo purposes. However it may take some time for Tiling to be completed and show up correctly.

A SimpleHTTPServer publishes the content of the _maps_ directory on http://localhost:8000/
